PhonePe.PhonePe.loggingEnabled = true;

var phonePeSDK;

toggleLoader();

PhonePe.PhonePe.build(PhonePe.Constants.Species.web)
	.then((sdk) => { 
  		phonePeSDK = sdk;
  		return login(sdk); 
	}).then((res) => {
		toggleLoader();
		alert("Welcome : " + res.name);
		userDetails = res;
		init(res);
	}).catch((err) => {
		alert(err);
	});

var userDetails;
var checkPaymentStatusThresholdCount = 5;

var login = function(sdk) {
	return new Promise(function(resolve, reject) {
		var userDetailsString = localStorage.getItem("userDetails");
		if (userDetailsString != null) {
			userDetails = JSON.parse(userDetailsString);
			resolve(userDetails);
		} else {
			sdk.fetchAuthToken()
				.then((res) => {
					return getUserDetails(res.grantToken);
				}).then((res) => {
					return setUserDetails(res);
				}).then((res) => {
					userDetails = res;
					resolve(res);
				}).catch((err) => {
					alert(err);
				});
		}
	});
}

var setUserDetails = function(response) {
	return new Promise(function(resolve, reject) {
		var userDetails = {};
		userDetails.userId = response.userId;
		userDetails.name = response.name;
		localStorage.setItem("userDetails", JSON.stringify(userDetails));
		resolve(userDetails);
	});
}

var getUserDetails = function(grantToken) {

	return new Promise(function(resolve, reject) {
		var requestBody = {};
		requestBody.token = grantToken;

		var request = JSON.stringify(requestBody);

		var xhttp = new XMLHttpRequest();
		xhttp.open("POST", "http://192.168.0.105:8001/user/sso/phonePe/getUserDetails", true);
		xhttp.setRequestHeader("Content-type", "application/json");
        xhttp.onload = () => {
			if (xhttp.readyState == 4 && xhttp.status == 200) {	
				resolve(JSON.parse(xhttp.responseText).userDetails);
			} else {
				alert("error : " + JSON.stringify(xhttp));
				reject(xhttp.statusText);
			}
		};
		xhttp.onerror = () => reject(xhttp.statusText);
		xhttp.send(request);
	});
};

function init(userDetails) {
	addEventListenerOnOrderButtons();
	addCollapsable();
	getOrdersAndPopulateOrderList(userDetails.userId);
}

function addEventListenersForCancelOrderButtons() {
	var paymentButtons = document.getElementsByClassName('cancel-order');
	for (var i = 0; i < paymentButtons.length; i++) {
		paymentButtons[i].addEventListener('click', function(){
			console.log(this.getAttribute("orderId"));
			cancelOrderAndPopulateOrderList(this.getAttribute("orderId"));
		});
	}
}

function addEventListenersPaymentButtons() {
	var paymentButtons = document.getElementsByClassName('pay-now');
	for (var i = 0; i < paymentButtons.length; i++) {
		paymentButtons[i].addEventListener('click', function(){
			console.log(this.getAttribute("orderId"));
			triggerPayment(this.getAttribute("orderId"));
		});
	}
}

function addEventListenerOnOrderButtons() {
	var placeOrderButtons = document.getElementsByClassName("placeOrder");
	for (var i = 0; i < placeOrderButtons.length; i++) {
		placeOrderButtons[i].addEventListener('click', function() {
			console.log(this.innerHTML);
			placeOrderAndTriggerPayment(userDetails.userId);
		});
	}
}

function showLoaderAndExecuteMethod(method) {
	toggleLoader();
	setTimeout(() => {
		method();
		toggleLoader();
	}, 1000);
}

function toggleLoader() {
	var loader = document.getElementById("loader").style.display;
	if (loader == "none") {
		document.getElementById("main-div").style.display = "none";
		document.getElementById("loader").style.display = "block";
	} else {
		document.getElementById("loader").style.display = "none";
		document.getElementById("main-div").style.display = "block";
	}
}

function triggerPayment(orderId) {
	var requestBody = JSON.stringify(generateInitiatePaymentRequestBody(orderId));
	console.log(requestBody);
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {	
			directPaymentRequestToPhonePe(orderId, this);
		}
	};
	xhttp.open("POST", "http://192.168.0.105:8001/payment/initiatePayment", true);
	xhttp.setRequestHeader("Content-type", "application/json");
	xhttp.send(requestBody);
}

function generateInitiatePaymentRequestBody(orderId) {
	var request = {};
	request.orderId = orderId;
	return request;
}

function directPaymentRequestToPhonePe(orderId, xhttp) {
	console.log(JSON.parse(xhttp.responseText).transactionId);
	console.log(xhttp.responseText);
	try {
		var reservationId = JSON.parse(xhttp.responseText).reservationId;
		var transactionId = JSON.parse(xhttp.responseText).transactionId;
		var redirectUrl = "http://192.168.0.105/msite";
		phonePeSDK.proceedToPay(reservationId, redirectUrl)
			.then((response) =>  console.log("Success"))
			.catch((err) => console.log(err))
			.finally(() => {
				var transactionId = JSON.parse(xhttp.responseText).transactionId;
				showLoaderAndExecuteMethod(() => checkPaymentStatusAndPopulateOrders(transactionId));
			});
	} catch(err1) {
		alert(err1);
	}
}

function checkPaymentStatusAndPopulateOrders(transactionId) {
	var paymentStatus = "PAYMENT_FAILED";
	for (var i=0; i<checkPaymentStatusThresholdCount; i++) {
		var response = checkPaymentStatus(transactionId);
		if (response.paymentStatus == 'PAYMENT_REJECTED') {
			break;
		} else if (response.paymentStatus == 'PAYMENT_SUCCESS') {
			paymentStatus = "PAYMENT_SUCCESS";
			break;
		}
	}
	alert(paymentStatus);
	getOrdersAndPopulateOrderList(userDetails.userId);
}

function checkPaymentStatus(transactionId) {
	var xhttp;
	xhttp = new XMLHttpRequest();
	xhttp.open("GET", "http://192.168.0.105:8001/payment/checkPaymentStatus?transactionId="+transactionId, false);
	xhttp.setRequestHeader("Content-type", "application/json");
	xhttp.send();
	return JSON.parse(xhttp.responseText);
}

function cancelOrderAndPopulateOrderList(orderId) {
	toggleLoader();
	var requestBody = {};
	requestBody.orderId = orderId;
	requestBody.cancellationReason = "Cancelled by user";
	console.log(requestBody);
	var cancelOrderUrl = "http://192.168.0.105:8001/order/cancelOrder";
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			alert("Order Cancelled");
			getOrdersAndPopulateOrderList(userDetails.userId);
			toggleLoader();
		}
	};
	xhttp.open("POST", cancelOrderUrl, true);
	xhttp.setRequestHeader("Content-type", "application/json");
	xhttp.send(JSON.stringify(requestBody));
}

function placeOrderAndTriggerPayment(userId) {
	toggleLoader();
	var order = {};
	order.userId = userId;
	order.cartItems = getCartItmesJson(); 
	placeOrder(JSON.stringify(order))
		.then((res) => {
			toggleLoader();
			triggerPayment(res.orderId);
		}).catch((err) => alert(err))
}

var placeOrder = function(order) {

	var placeOrderUrl = "http://192.168.0.105:8001/order/placeOrder";

	return new Promise(function(resolve, reject) {
		var xhttp = new XMLHttpRequest();
		xhttp.open("PUT", placeOrderUrl, true);
		xhttp.setRequestHeader("Content-type", "application/json");
        xhttp.onload = () => {
			if (xhttp.readyState == 4 && xhttp.status == 200) {	
				resolve(JSON.parse(xhttp.responseText));
			} else {
				alert("error : " + JSON.stringify(xhttp));
				reject(xhttp.statusText);
			}
		};
		xhttp.onerror = () => reject(xhttp.statusText);
		xhttp.send(order);
	});
};

function getOrdersAndPopulateOrderList(userId) {
	var getOrderRequestUrl = "http://192.168.0.105:8001/order/getOrders";
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var response = parseGetOrdersResponse(this);
			populateOrderList(response.orders);
		}
	};
	xhttp.open("GET", getOrderRequestUrl+"?userId="+userId, true);
	xhttp.setRequestHeader("Content-type", "application/json");
	xhttp.send();
}

function parseGetOrdersResponse(xhttp) {
	var response = JSON.parse(xhttp.responseText);
	return response;
}

function populateOrderList(orderList) {
	var activeOrderRowTemplate = "<div class=\"order-tile-div\">$descrtiption$<button type=\"button\" class=\"cancel-order\" orderId=\"$orderId$\">Cancel</button></div><br>";
	var failedOrderRowTemplate = "<div class=\"order-tile-div\">$descrtiption$<button type=\"button\" class=\"pay-now\" orderId=\"$orderId$\">Retry Payment!</button></div><br>";
	var cancelledOrderRowTemplate = "<div class=\"order-tile-div\">$descrtiption$</div>";
	var completedOrderRowTemplate = "<div class=\"order-tile-div\">$descrtiption$</div><br>";

	var activeOrderRowHtml = "";
	var failedOrderRowHtml = "";
	var cancelledOrderRowHtml = "";
	var completedOrderRowHtml = "";

	for (var i = 0; i < orderList.length; i++) {
		var order = orderList[i];
		switch(order.status) {
			case 'COMPLETED' : 	completedOrderRowHtml += getRowHtml(completedOrderRowTemplate, order.items, order.orderId);
								break;
			case 'FAILED' : 	failedOrderRowHtml += getRowHtml(failedOrderRowTemplate, order.items, order.orderId);
								break;
			case 'IN_PROGRESS_FOR_PAYMENT_CANCELLATION' :
			case 'PAYMENT_SUCCESS' 	:activeOrderRowHtml += getRowHtml(activeOrderRowTemplate, order.items, order.orderId);
									break;
			case 'CANCELLED' : 	cancelledOrderRowHtml += getRowHtml(cancelledOrderRowTemplate, order.items, order.orderId);
								break;
		}
	}
	var activeOrdersDiv = document.getElementById('active-order-list-div');
	var failedOrdersDiv = document.getElementById('failed-order-list-div');
	var cancelledOrdersDiv = document.getElementById('cancelled-order-list-div');
	var completedOrdersDiv = document.getElementById('completed-order-list-div');

	activeOrdersDiv.innerHTML = activeOrderRowHtml;
	failedOrdersDiv.innerHTML = failedOrderRowHtml;
	cancelledOrdersDiv.innerHTML = cancelledOrderRowHtml;
	completedOrdersDiv.innerHTML = completedOrderRowHtml;
	addEventListenersPaymentButtons();
	addEventListenersForCancelOrderButtons();
}

function getRowHtml(rowHtmlTemplate, itemList, orderId) {
	var itemlListHtml = getItemListHtml(itemList);
	var rowHtml = rowHtmlTemplate.replace("$descrtiption$", itemlListHtml);
	return rowHtml.replace("$orderId$", orderId);
}

function getItemListHtml(itemList) {
	var itemListHtmlTemplate = "<div>$itemName$</div><br>"
	var itemListHtml = "";
	for (var i = 0; i < itemList.length; i++) {
		var item = itemList[i];
		var rowHtml =  itemListHtmlTemplate.replace("$itemName$", item.itemName);
		itemListHtml += rowHtml;
	}
	return itemListHtml;
}

var temp ;

function updateQuantity(delta,obj) {
	temp  = obj;
	console.log(delta);
	var itemDescriptionTemplate = "$itemName$ X $quantity$ : Rs $totalAmount$";
	var parent = obj.parentElement;
	var quantity = parseInt(parent.getAttribute("quantity"));
	if (quantity +  delta <= 0) {
		return;
	}
	quantity += delta;
	var itemName = parent.getAttribute("itemName");
	var price = parseInt(parent.getAttribute("price"));
	var itemDescription = itemDescriptionTemplate.replace("$itemName$",itemName).replace("$quantity$",quantity).replace("$totalAmount$", quantity*price);
	var ele = parent.getElementsByTagName("span");
	for (var i = 0; i < ele.length; i++) {
		ele[i].innerHTML = itemDescription;
	}
	parent.setAttribute("quantity", quantity);
	updateTotalCartAmount(delta);
}

function updateTotalCartAmount(delta) {
	var totalAmountElement = document.getElementById("cart-total");
	var totalAmount = parseInt(totalAmountElement.getAttribute("val"));
	totalAmount += parseInt(delta);
	totalAmountElement.innerHTML = "Total Amount :	Rs " + totalAmount;
	totalAmountElement.setAttribute("val", totalAmount);
}

function getCartItmesJson() {
	var itemTemplate = "{\"category\":\"SHOPPING\",\"itemId\":\"$itemId$\",\"price\":$price$,\"itemName\":\"$itemName$\",\"quantity\":$quantity$,\"shippingDetails\":{\"deliveryType\":\"STANDARD\",\"expectedDeliveryEndTime\":\"2020-04-24T15:49:43.000\",\"shippingAddress\":{\"addressString\":\"TEST\",\"city\":\"TEST\",\"pincode\":\"TEST\",\"country\":\"TEST\",\"latitude\":1,\"longitude\":1}}}";
	var itemList = [];
	var itemEements = document.getElementsByClassName("item-div");
	for (var i = 0; i < itemEements.length; i++) {
		var item = itemEements[i];
		var itemId = item.getAttribute("itemId");
		var itemName = item.getAttribute("itemName");
		var price = parseInt(item.getAttribute("price"));
		var quantity = item.getAttribute("quantity");
		var itemJson = itemTemplate.replace("$itemId$",itemId).replace("$price$",price).replace("$itemName$", itemName).replace("$quantity$", quantity);
		itemList.push(JSON.parse(itemJson));
	}
	return itemList;
}

function addCollapsable() {
	var coll = document.getElementsByClassName("collapsible");
	for (var i = 0; i < coll.length; i++) {
		coll[i].addEventListener("click", function() {
			this.classList.toggle("active");
			var content = this.nextElementSibling;
			if (content.style.maxHeight){
				content.style.maxHeight = null;
			} else {
				content.style.maxHeight = content.scrollHeight + "px";
			} 
		});
	}
}